<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('tugas.register');
    }

    public function welcome(Request $request){
        $nama1 = $request['name'];
        $nama2 = $request['nama'];
        return view('tugas.welcome', compact('nama1','nama2'));
    }
}
