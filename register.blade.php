<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Silahkan Buat Account Baru</h2>
    <h3 >Sign Up form</h3>
    <form action="/form" method="post">
        @csrf
        <label> First Name </label> <br>
        <input type="text" name="name"> <br><br>
        <label> Last Name </label> <br>
        <input type="text" name="nama"> <br><br>
        <label> Gender </label> <br>
        <input type="radio" name="jk"> Male <br>
        <input type="radio" name="jk"> Female <br> <br>
        <label> Nationality </label> <br>
        <select name="negara" id=""> <br>
            <option value="1"> Indonesia </option>
            <option value="2"> Amerika </option>
            <option value="3"> Malaysia </option>
            <option value="4"> Korea </option>
            <option value="5"> Singapura </option>
        </select> <br> <br>
        <label> Language Spoken </label> <br>
        <input type="checkbox" name="bahasa"> Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa"> English <br>
        <input type="checkbox" name="bahasa"> Other <br> <br>
        <label> Bio </label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>